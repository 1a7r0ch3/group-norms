# Group Norms and Proximity Operators
Routines in GNU Octave or MATLAB, with some C/C++ support with mex API.
Parallel implementation with OpenMP API.

## Available Routines

### 2D grid block structure
Create list of indices constituting a regular nonoverlapping two dimensional block structure: mex `grid_2D_groups`  
Create all regular nonoverlapping two dimensional block structures over a given domain: `all_grids_2D_groups.m`

### Group norms
Compute the group norm defined by the given group structure. 
ℓ<sub>1,2</sub>: mex `norm_l12`  
δ<sub>1,2</sub>: mex `norm_d12`  
ℓ<sub>1,∞</sub>: mex `norm_l1inf`  
δ<sub>1,∞</sub>: mex `norm_d1inf`  

### Norm of each group
Compute the norm of each group in the given group structure. 
The two methods supports _p_ ∈ ℝ<sub>+</sub> or _p_ = +∞.
Depend on `norms_wl1p.c`.  
ℓ<sub>1,_p_</sub>: mex `group_norms_l1p`  
δ<sub>1,_p_</sub>: mex `group_norms_d1p`  

### Proximity operators
Compute the proximity operator of some group norms, of some convex indicators of (yielding orthogonal projector to) sets of bounded group norms, and of some of their linear combinations. Group structures must be nonoverlapping.

Proximity operator of the ℓ<sub>1,2</sub> norm: mex `prox_l12`  
Proximity operator of the ℓ<sub>1,2</sub> bound (i.e. projection on the ℓ<sub>1,2</sub> ball): mex `proj_l12`  
Proximity operator of the reweighted ℓ<sub>1,2</sub> norm: mex `prox_rwl12`  
Proximity operator of the ℓ<sub>1,∞</sub> norm: mex `prox_l1inf`  
Proximity operator of the ℓ<sub>1,∞</sub> bound (i.e. projection on the ℓ<sub>1,∞</sub> ball): mex `proj_l1inf`  
Proximity operator of the δ<sub>1,2</sub> semi-norm (used for *total variation* semi-norm): mex `prox_d12`  
Proximity operator of the δ<sub>1,2</sub> bound (i.e. projection on the δ<sub>1,2</sub> ball): mex `proj_l12`  
Proximity operator of the reweighted δ<sub>1,2</sub> semi-norm: mex `prox_rwd12`  
Proximity operator of a sum of a ℓ<sub>1,2</sub> norm and a ℓ<sub>1,2</sub> bound: mex `proxj_l12`  
Proximity operator of a sum of a δ<sub>1,2</sub> semi-norm and a δ<sub>1,2</sub> bound: mex `proxj_d12`  
Proximity operator of a sum of a ℓ<sub>1,2</sub> norm and a δ<sub>1,2</sub> semi-norm (generalization of *fused lasso*): mex `prox_l12_d12`  

## Mex Routines

Within directory mex/  
    C/C++ sources are in directory src/  
    headers are in directory include/  
    Mex API are in directory api/  
    some documentation in doc/  

Typical UNIX compilation command for `<name>_mex` (in directory mex/, create directory bin/):  
`mex CXXFLAGS="\$CXXFLAGS -DMEX -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp" api/<name>_mex.cpp src/<name>.cpp -output bin/<name>_mex`

## Documentation
A group structure is an array (cell array in Octave or MATLAB) of arrays of coordinate indices. Since C/C++ routines are extensively used, indices starts at 0, and metadata concerning the number of elemennts in the arrays are given. More precisely, the group structure `G` is such that:  
`G[0]` points to an array containing the number of groups (`G[0][0]`) and the maximum group size (`G[0][1]`).  
`G[i]` points to an array containing the number of elements in the i-th group (`G[i][0]`) and the indices of the coordinates in the i-th group  (`G[i][1]` to `G[i][G[i][0]-1]`).  
When working with a group structure containing overlapping groups, usually the group is represented by an array (cell array in Octave or MATLAB) or nonoverlapping group structures; see in particular `all_grids_2D_groups.m`.

## References
H. Raguet, "A Signal Processing Approach to VSDOI", Chapter V, Ph.D. Thesis, 2014
