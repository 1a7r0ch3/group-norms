function G = all_grids_2D_groups(ROI, sz, sp);
%
%	     G = all_grids_2D_groups([ROI|Xsz], sz, [sp=1]);
%
% Hugo Raguet 2015
if nargin < 3, sp = 1; end
if length(sz) == 1, sz = [sz sz]; end
if length(sp) == 1, sp = [sp sp]; end

nb = ceil(sz./sp);
G = cell(1, prod(nb));
for i=1:nb(1), for j=1:nb(2)
	sh = ([i j]-1).*sp;
    G{i+nb(1)*(j-1)} = grid_2D_groups_mex(ROI, sz(1), sz(2), sh(1), sh(2));
end, end

end %all_grids_2D_groups
